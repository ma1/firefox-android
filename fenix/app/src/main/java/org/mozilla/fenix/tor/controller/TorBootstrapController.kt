/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor.controller

import org.mozilla.fenix.tor.interactor.TorBootstrapInteractor

interface TorBootstrapController {
    /**
     * @see [TorBootstrapInteractor.onTorBootstrapConnectClicked]
     */
    fun handleTorBootstrapConnectClicked()

    /**
     * @see [TorBootstrapInteractor.onTorStopBootstrapping]
     */
    fun handleTorStopBootstrapping()

    /**
     * @see [TorBootstrapInteractor.onTorStartBootstrapping]
     */
    fun handleTorStartBootstrapping()

    /**
     * @see [TorBootstrapInteractor.onTorStartDebugBootstrapping]
     */
    fun handleTorStartDebugBootstrapping()

    /**
     * @see [TorBootstrapInteractor.onTorBootstrapNetworkSettingsClicked]
     */
    fun handleTorNetworkSettingsClicked()


}

class DefaultTorBootstrapController(
    private val handleTorBootstrapConnect: () -> Unit,
    private val initiateTorBootstrap: (Boolean) -> Unit,
    private val cancelTorBootstrap: () -> Unit,
    private val openTorNetworkSettings: () -> Unit
) : TorBootstrapController {
    override fun handleTorBootstrapConnectClicked() {
        handleTorBootstrapConnect()
    }

    override fun handleTorStopBootstrapping() {
        cancelTorBootstrap()
    }

    override fun handleTorStartBootstrapping() {
        initiateTorBootstrap(false)
    }

    override fun handleTorStartDebugBootstrapping() {
        initiateTorBootstrap(true)
    }

    override fun handleTorNetworkSettingsClicked() {
        openTorNetworkSettings()
    }
}
