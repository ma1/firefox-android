/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import org.mozilla.fenix.BuildConfig
import org.mozilla.fenix.databinding.FragmentHomeBinding
import org.mozilla.fenix.ext.requireComponents
import org.mozilla.fenix.tor.bootstrap.TorQuickStart
import org.mozilla.fenix.tor.interactor.DefaultTorBootstrapInteractor
import org.mozilla.fenix.tor.interactor.TorBootstrapInteractor
import androidx.navigation.fragment.findNavController
import com.google.android.material.appbar.AppBarLayout
import org.mozilla.fenix.ext.components
import org.mozilla.fenix.ext.hideToolbar
import org.mozilla.fenix.tor.controller.DefaultTorBootstrapController
import org.mozilla.fenix.tor.view.TorBootstrapView


@Suppress("TooManyFunctions", "LargeClass")
class TorBootstrapFragment : Fragment() {
    private val torQuickStart by lazy { TorQuickStart(requireContext()) }

    internal var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!


    private var torBootstrapView: TorBootstrapView? = null

    private var _torBootstrapInteractor: TorBootstrapInteractor? = null
    private val torBootstrapInteractor: TorBootstrapInteractor
        get() = _torBootstrapInteractor!!

    private lateinit var torBootstrapStatus: TorBootstrapStatus


    @Suppress("LongMethod")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val components = requireComponents

        torBootstrapStatus = TorBootstrapStatus(
            torQuickStart,
            !BuildConfig.DISABLE_TOR,
            components.torController,
            ::dispatchModeChanges
        )

        if (!torBootstrapStatus.isBootstrapping()) {
            openHome()
        }

        // Was _sessionControlInteractor
        _torBootstrapInteractor = DefaultTorBootstrapInteractor(
            controller = DefaultTorBootstrapController(
                handleTorBootstrapConnect = ::handleTorBootstrapConnect,
                cancelTorBootstrap = ::cancelTorBootstrap,
                initiateTorBootstrap = ::initiateTorBootstrap,
                openTorNetworkSettings = ::openTorNetworkSettings
            ),
        )

        torBootstrapView = TorBootstrapView(
            containerView = binding.sessionControlRecyclerView,
            viewLifecycleOwner = viewLifecycleOwner,
            interactor = torBootstrapInteractor,
        )

        adjustHomeFragmentView()
        updateSessionControlView()
        showSessionControlView()

        return binding.root
    }

    private fun updateSessionControlView() {
        torBootstrapView?.update(requireContext().components.appStore.state)
    }

    // This function should be paired with showSessionControlView()
    private fun adjustHomeFragmentView() {
        binding.sessionControlRecyclerView.apply {
            visibility = View.INVISIBLE
        }

        binding.sessionControlRecyclerView.apply {
            setPadding(0, 0, 0, 0)
            (layoutParams as ViewGroup.MarginLayoutParams).setMargins(0, 0, 0, 0)
        }

        binding.homeAppBar.apply {
            visibility = View.GONE

            // Reset this as SCROLL in case it was previously set as NO_SCROLL after bootstrap
            children.forEach {
                (it.layoutParams as AppBarLayout.LayoutParams).scrollFlags =
                    AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
            }
        }
        binding.onionPatternImage.apply {
            visibility = View.GONE
        }
        binding.toolbarLayout.apply {
            visibility = View.GONE
        }

        binding.yecPopup.apply {
            visibility = View.GONE
        }
    }

    // This function should be paired with adjustHomeFragmentView()
    private fun showSessionControlView() {
        binding.sessionControlRecyclerView.apply {
            visibility = View.VISIBLE
        }
    }

    private fun dispatchModeChanges(isBootstrapping: Boolean) {
        //requireComponents.appStore.dispatch(AppAction.ModeChange(mode))
        if (!isBootstrapping) {
            openHome()
        } else {
            adjustHomeFragmentView()
            updateSessionControlView()
            showSessionControlView()
        }
    }

    override fun onStop() {
        super.onStop()
        torBootstrapStatus.unregisterTorListener()
    }

    override fun onResume() {
        super.onResume()

        torBootstrapStatus.registerTorListener()

        // fenix#40176: Ensure the Home fragment is rendered correctly when we resume.
        val isBootstraping = torBootstrapStatus.isBootstrapping()

        if (!isBootstraping) {
            openHome()
        }

        adjustHomeFragmentView()
        updateSessionControlView()
        showSessionControlView()

        hideToolbar()

        // Whenever a tab is selected its last access timestamp is automatically updated by A-C.
        // However, in the case of resuming the app to the home fragment, we already have an
        // existing selected tab, but its last access timestamp is outdated. No action is
        // triggered to cause an automatic update on warm start (no tab selection occurs). So we
        // update it manually here.
        requireComponents.useCases.sessionUseCases.updateLastAccess()
    }

    private fun handleTorBootstrapConnect() {
        requireComponents.torController.onTorConnecting()
    }

    private fun cancelTorBootstrap() {
        requireComponents.torController.stopTor()
    }

    private fun initiateTorBootstrap(withDebugLogging: Boolean = false) {
        requireComponents.torController.initiateTorBootstrap(lifecycleScope, withDebugLogging)
    }

    private fun openTorNetworkSettings() {
        val directions =
            TorBootstrapFragmentDirections.actionTorbootstrapFragmentToTorNetworkSettingsFragment()
        findNavController().navigate(directions)
    }

    private fun openHome() {
        val directions =
            TorBootstrapFragmentDirections
                .actionStartupHome()
        findNavController().navigate(directions)
    }

}
