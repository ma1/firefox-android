/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor.view

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.components.appstate.AppState
import org.mozilla.fenix.ext.components
import org.mozilla.fenix.tor.interactor.TorBootstrapInteractor


class TorBootstrapView(
    containerView: RecyclerView,
    viewLifecycleOwner: LifecycleOwner,
    interactor: TorBootstrapInteractor,
) {

    val view: RecyclerView = containerView //as RecyclerView

    private fun bootstrapAdapterItems() = listOf(AdapterItem.TorBootstrap)

    private val torBootstrapAdapter = TorBootstrapAdapter(
        interactor,
        viewLifecycleOwner,
        containerView.context.components,
    )

    //private val torBootstrapAdapter =
    //    TorBootstrapAdapter(interactor, containerView.context.components)
    //private val torBootstrapAdapter = TorBootstrapPagerAdapter(containerView.context.components, interactor)

    init {
        containerView.apply {
            adapter = torBootstrapAdapter
            layoutManager = LinearLayoutManager(containerView.context)
        }
    }

    private fun AppState.toAdapterList(): List<AdapterItem> {
        return bootstrapAdapterItems()
    }

    fun update(state: AppState) {
        torBootstrapAdapter.submitList(state.toAdapterList())
    }
}
